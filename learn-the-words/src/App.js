import React, {Component} from 'react';
import Login from './pages/Login';
import Home from './pages/Home';
import FirebaseContext from './context/firebaseContext';
import Register from './pages/Registration';
import {BrowserRouter, Route} from 'react-router-dom';
import {PrivateRoute, LoggedRoute} from './utils/privateRoute';
import {connect} from 'react-redux';
import { addUserAction } from './actions/userAction';
import {bindActionCreators} from 'redux'

class App extends Component {
/*     state ={
        user: null,
        register: false,
    } */

    componentDidMount(){
        const {auth, setUid} = this.context;
        const {addUser} = this.props;
        auth.onAuthStateChanged((user) => {
            if (user)
            {
                setUid();
                localStorage.setItem('user',JSON.stringify(user.uid));
                addUser(user);
/*                 this.setState({
                    user,
                }) */
            } else {
                localStorage.removeItem('user');
                console.log('signing out');
//                signOut();
/*                 this.setState({
                    user: false,
                    
                }) */
            }
        })
        
    }
    render(){
         const {userUid} = this.props;
/*         return(
            <>
                {user ? <Home/> : (register ? <Register registred = {()=>this.setState({register:false})}/> : <Login toReg = {()=>this.setState({register:true})}/>)}
            </>
        ) */
        return(
            <BrowserRouter> 
            <PrivateRoute path="/" exact component={Home}/>
            <PrivateRoute path="/home" component={Home}/>
            <LoggedRoute path="/login" component={Login}/>
            <Route path="/register" component={Register}/>

                </BrowserRouter>
        )
    }
}

App.contextType=FirebaseContext;


const mapStateToProps = (state) =>
{
    return({
        userUid:state.user.userUid,
        userEmail:state.user.email,
    })
}


const mapDispatchToProps = (dispatch) =>
{
    return bindActionCreators({
        addUser:addUserAction,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);

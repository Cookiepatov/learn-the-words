import * as actionTypes from './actionTypes';

const {PLUS, MINUS} = actionTypes

export const plusAction = (amount) =>
{
    return{
        type: PLUS,
        payload: amount,
    }
}

export const minusAction = (amount) =>
{
    return{
        type: MINUS,
        payload: amount,
    }
}
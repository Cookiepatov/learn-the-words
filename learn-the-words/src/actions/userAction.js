import * as actionTypes from './actionTypes';

const {ADD_USER} = actionTypes

export const addUserAction = (user) =>({
    type: ADD_USER,
    user,
})

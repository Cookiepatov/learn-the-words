import React from 'react'
import cl from 'classnames';
import { CheckSquareOutlined, DeleteOutlined } from '@ant-design/icons';

import s from './Card.module.scss';

class Card extends React.Component {
    state = {
        done: false,
        isRemembered: false
    }

    handleCardClick = () => {
        if(!this.state.isRemembered){
            this.setState((state) => {return {done: !state.done}});
        }
        
    }

    handleIsRememberedClick = () => {
        this.setState((state) =>{ return {isRemembered: !state.isRemembered}});
        if(!this.state.done)
        {
            this.handleCardClick();
        }
    }
    handleDeletedClick =() => {
        this.props.onDeleted();

    }

    render() {
        const {en, ru} = this.props
        const { done, isRemembered } = this.state        
        return (
            <div className={s.root}>
                <div
                    className={ cl(s.card, { 
                        [s.done]: done,
                        [s.isRemembered]: isRemembered
                    }) }
                    onClick={ this.handleCardClick}
                >
                    <div className={s.cardInner}>
                        <div className={s.cardFront}>
                            {en}
                        </div>
                        <div className={s.cardBack}>
                            {ru}
                        </div>
                    </div>
                </div>
                <div className={s.icons}
                >
                    <CheckSquareOutlined onClick={ this.handleIsRememberedClick}/>
                    
                </div>
                <div className={cl(s.icons, s.deleted)}>
                    <DeleteOutlined onClick={this.handleDeletedClick}/>
                </div>
            </div>
        )
    }

}


export default Card;

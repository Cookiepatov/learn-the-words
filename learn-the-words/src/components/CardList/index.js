import React, { Component, createRef} from 'react';
import Card from '../Card';
import getTranslatedWord from '../../services/dictionary'

import s from './CardList.module.scss';

class CardList extends Component {
    state = {
        inputE: '',
        inputR: '',
    }
    inputRef = createRef();
    setFocus =()=>
    {
        this.inputRef.current.focus();
    }
    handleInputChangeR=(e)=>{
        this.setState({inputR : e.target.value})
        
    }
    handleInputChangeE=(e)=>{
        this.setState({inputE : e.target.value})
        
    }
    handleButtonClick = async (e)=>
    {
        this.setFocus();
        e.preventDefault();
        let inputRes={...this.state}
        if (inputRes.inputE==='')
        {
            if (inputRes.inputR==='')
            {
                return
            }
            const getWord= await getTranslatedWord(inputRes.inputR,'ru-en');
            inputRes.inputE=getWord;
        }
        else{
            const getWord= await getTranslatedWord(inputRes.inputE,'en-ru');
            inputRes.inputR=getWord;
        }
        this.props.onAdded(inputRes)
        this.setState({inputE : '', inputR : ''})
    }
    render() {
        const { items } = this.props;
        
        return (
            <>
            <form
            className={s.form}
            onSubmit={this.handleButtonClick}>
                <input 
                ref={this.inputRef}
                type="text"
                placeholder="Русское слово"
                value={this.state.inputR}
                onChange={this.handleInputChangeR}
                />
                <input
                type="text"
                placeholder="Английский перевод"
                value={this.state.inputE}
                onChange={this.handleInputChangeE}
                />
                <button>
                    Добавить слово
                </button>

            </form>
                <div className={s.root}>
                    {
                        items.map(({ en, ru, id }) => (
                            <Card
                                onDeleted = {()=>this.props.onDeletedItem(id)}
                                key={id}
                                en={en}
                                ru={ru}
                            />
                        ))
                    }
                </div>
            </>
        );
    }
}

export default CardList;

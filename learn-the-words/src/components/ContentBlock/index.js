import React from 'react';
import s from './ContentBlock.module.css';

const ContentBlock = ({text1, text2}) =>{
    return (
        <div>
            <div>
                <h1 className={s.header}>{text1}</h1>
                <p>{text2}</p>
            </div>
        </div>
    )
}


export default ContentBlock;
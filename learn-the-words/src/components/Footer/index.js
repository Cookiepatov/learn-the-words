import React from 'react';
import s from './Footer.module.scss';

const Footer = () => {
    return (
        <footer className={s.root}>
            Made mostly by Zar Zakharov 
        </footer>
    );
};

export default Footer;
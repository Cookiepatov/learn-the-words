import React from 'react';
import s from './FooterBlock.module.css';

const FooterBlock = ({author}) =>{
    return (
        <div>
            <div>
                <p className={s.header}>@{author}</p>
            </div>
        </div>
    )
}


export default FooterBlock;
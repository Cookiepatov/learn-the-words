import React from 'react';
import s from './HeaderBlock.module.css';
import pic from '../../rick.gif';

const HeaderBlock = ({title='', hideBackground = false, isImg = false}) =>{
    const styleCover = hideBackground ? {backgroundImage: 'none'} : {};
    if (isImg)
    {
        return(
        <div>
            <img src={pic}/>
        </div>
        )
    }
    return (
    <div className="cover">
        <div className="wrap">
            <h1 className={s.header} style={styleCover}>{title}</h1>
        </div>
    </div>
    )
}


export default HeaderBlock;
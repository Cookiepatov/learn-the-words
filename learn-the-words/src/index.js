import React from 'react';
import ReactDom from 'react-dom';
import App from './App';
import FirebaseContext from './context/firebaseContext'
import Firebase from './services/firebase'
import './index.css';
import {Provider} from 'react-redux'
import {applyMiddleware, createStore, compose} from 'redux';
import { BrowserRouter } from 'react-router-dom';
import rootReducer from './reducers'
import thunk from 'redux-thunk'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = new createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));


ReactDom.render(
    <Provider store={store}>
    <FirebaseContext.Provider value={new Firebase()}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
        
    </FirebaseContext.Provider> </Provider>, document.getElementById('root'));
import React, {Component, createRef} from 'react';
import { ClockCircleOutlined, HomeOutlined, SmileOutlined } from '@ant-design/icons';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import BackgroundBlock from '../../components/BackgroundBlock';
import Button from '../../components/Button';
import CardList from '../../components/CardList';
import Footer from '../../components/Footer';
import Header from '../../components/Header';
import Paragraph from '../../components/Paragraph';
import Section from '../../components/Section';
import firstBackground from '../../assets/background.jpg';
import secondBackground from '../../assets/back2.jpg';
import s from './Home.module.scss';
import FirebaseContext from '../../context/firebaseContext';
import {fetchCardList} from '../../actions/cardListAction';
class Homepage extends Component {
    state = {
        wordArr: [],
    }
    cardsRef = createRef();
    
    componentDidMount() {
        const {getCards} = this.context;
        const {fetchCardList} = this.props
        fetchCardList(getCards);
    }

/*     componentDidUpdate()
    {
        const {items} = this.props
        if (items.length>0&&(this.state.wordArr!=items))
        {
            console.log('items = ',items);
                this.setState({
                wordArr: items
            })  
        }
    } */
    handleDeletedItem =(id) =>
    {
        const {deleteCard} = this.context;
        deleteCard(id,this.state.wordArr);


    }
    handleAddedItem =(inputE, inputR) =>
    {
        const {addCard}=this.context;
        addCard(inputE,inputR,this.state.wordArr);
    }

    handleSignOut =() =>
    {
        this.context.signOut();
    }
    handleGoDown =()=>
    {
        this.cardsRef.current.setFocus();
    }
    render() {
        const {items} = this.props
        const {wordArr} = this.state
        console.log('home props', this.props);
        if (items.length>0&&(this.state.wordArr!=items))
        {
            console.log('items = ',items);
                this.setState({
                wordArr: items
            })  
        }
        return (
            <>
            <BackgroundBlock
                backgroundImg={firstBackground}
                fullHeight
            >
                <Paragraph white>
                    Вы вошли как {this.props.userEmail}
                </Paragraph>
                <Header white>
                    Время учить слова онлайн
                </Header>
                <Paragraph white>
                    Используйте карточки для запоминания и пополняйте активный словарный запас.
                </Paragraph>
                <button onClick={this.handleGoDown} > 
                    Вперёд!
                </button>
                <button onClick={this.handleSignOut}>
                    Выйти
                </button>
            </BackgroundBlock>
            <Section className={s.textCenter}>
                <Header size="l">
                    Мы создали уроки, чтобы помочь вам увереннее разговаривать на английском языке
                </Header>
                <div className={s.motivation}>
                    <div className={s.motivationBlock}>
                        <div className={s.icons}>
                            <ClockCircleOutlined /> 
                        </div>
                        <Paragraph small>
                            Учитесь, когда есть свободная минутка
                        </Paragraph>
                    </div>

                    <div className={s.motivationBlock}>
                        <div className={s.icons}>
                            <HomeOutlined />
                        </div>
                        <Paragraph small>
                            Откуда угодно — дома, в&nbsp;офисе, в&nbsp;кафе
                        </Paragraph>
                    </div>

                    <div className={s.motivationBlock}>
                        <div className={s.icons}>
                            <SmileOutlined />
                        </div>
                        <Paragraph small>
                            Разговоры по-английски без&nbsp;неловких пауз и&nbsp;«mmm,&nbsp;how&nbsp;to&nbsp;say…»
                        </Paragraph>
                    </div>
                </div>
            </Section>
            <Section bgColor="#f0f0f0" className={s.textCenter}>
                <Header size='l'>
                    Начать учить английский просто
                </Header>
                <Paragraph>
                    Клика по карточкам и узнавай новые слова, быстро и легко!
                </Paragraph>

                <CardList 
                    ref={this.cardsRef}
                    items={wordArr}
                    onDeletedItem={(id)=>this.handleDeletedItem(id)}
                    onAdded={({inputE, inputR})=>this.handleAddedItem(inputE, inputR)}
                />
            </Section>
            <BackgroundBlock
                backgroundImg={secondBackground}
            >
                <Header size="l" white>
                    Изучайте английский с персональным сайтом помощником
                </Header>
                <Paragraph white>
                    Начните прямо сейчас
                </Paragraph>
                <Button>
                    Начать бесплатный урок
                </Button>
            </BackgroundBlock>
            <Footer/>
            </>
        );
    }
}
Homepage.contextType = FirebaseContext

const mapStateToProps = (state) =>
{
    return({
            userUid:state.user.userUid,
            userEmail:state.user.email,
            isBusy: state.cardList.isBusy,
            items: state.cardList.payload || [],
    })
}


const mapDispatchToProps = (dispatch) =>
{
    return bindActionCreators({
        fetchCardList:fetchCardList,
    }, dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(Homepage)
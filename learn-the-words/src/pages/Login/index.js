import {Layout, Form, Input, Button, Checkbox } from 'antd';
import React, {Component} from 'react';
import s from './Login.module.scss'
import FirebaseContext from '../../context/firebaseContext'
import {BrowserRouter, Route, Link} from 'react-router-dom'


class Login extends Component
{
    onFinish = async (values) => {
        const id = await this.context.singInWithEmail(values.username, values.password);
        localStorage.setItem('user',JSON.stringify(id));
    };
    handleRegister = () =>
    {
        this.props.toReg()
    }

    renderForm = () =>{
        
        const layout = {
            labelCol: { span: 8 },
            wrapperCol: { span: 16 },
        };
        const tailLayout = {
            wrapperCol: { offset: 8, span: 16 },
        };
        return(
            <Form
            {...layout}
            name="basic"
            initialValues={{ remember: true }}
            onFinish={this.onFinish}
            onFinishFailed={this.onFinishFailed}
        >
        <Form.Item
            label="Username"
            name="username"
            rules={[{ required: true, message: 'Please input your username!' }]}
        >
        <Input />
        </Form.Item>

        <Form.Item
            label="Password"
            name="password"
            rules={[{ required: true, message: 'Please input your password!' }]}
        >
        <Input.Password />
        </Form.Item>

        <Form.Item {...tailLayout} name="remember" valuePropName="checked">
        <Checkbox>Remember me</Checkbox>
        </Form.Item>

        <Form.Item {...tailLayout}>
        <Button type="primary" htmlType="submit">
            Submit
        </Button>
        </Form.Item>

        </Form>
        
        )
    }
    onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
        localStorage.removeItem('user');
    };
    render()
    {
        const {Content} = Layout;
        return (
            <BrowserRouter> 
               <Layout>
                   <Content>
                       <div className={s.root}>
                        <div className={s.form_wrap}>
                            {this.renderForm()}
                            <button>Регистрация
                                <Link to="/register"/>
                            </button>
                        </div>
                        
                        </div>
                        
                   </Content>
               </Layout>
               </BrowserRouter> 
            )
    }
      
}
Login.contextType=FirebaseContext
export default Login
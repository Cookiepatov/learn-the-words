import countReducer from './countReducer';
import userReducer from './userReducer'
import cardListReducer from './cardListReducer'
import {combineReducers} from 'redux';
export default combineReducers({
    user: userReducer,
    counter: countReducer,
    cardList: cardListReducer,
});
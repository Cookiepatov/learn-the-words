import * as actionTypes from '../actions/actionTypes';
const {ADD_USER} = actionTypes


const userReducer =(state = {}, action)=>
{
    switch (action.type) {
        case ADD_USER:
            return{
                ...state,
                userUid: action.user.uid,
                name: action.user.displayName,
                email: action.user.email,
                
            }
        default:
            return state;
    }
}


export default userReducer;
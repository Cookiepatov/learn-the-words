const getTranslatedWord = async (text, lang) => {
    const res = await fetch( `https://reactmarathon-api.netlify.app/api/translate?text=${text}&lang=${lang}`,{
        headers: {
            'Authorization': process.env.REACT_APP_API_KEY,
        }
    })
    const body = await res.json();
    return body.translate
}


export default getTranslatedWord;
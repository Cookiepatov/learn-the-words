import * as firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth'
const FIREBASE_CONFIG={
    apiKey: process.env.REACT_APP_FIREBASE_CONFIG_apiKey,
    authDomain: process.env.REACT_APP_FIREBASE_CONFIG_authDomain,
    databaseURL: process.env.REACT_APP_FIREBASE_CONFIG_databaseURL,
    projectId: process.env.REACT_APP_FIREBASE_CONFIG_projectId,
    storageBucket: process.env.REACT_APP_FIREBASE_CONFIG_storageBucket,
    messagingSenderId: process.env.REACT_APP_FIREBASE_CONFIG_messagingSenderId,
    appId: process.env.REACT_APP_FIREBASE_CONFIG_appId
}


class Firebase{
    constructor()
    {
        firebase.initializeApp(FIREBASE_CONFIG);

        this.auth = firebase.auth();
        this.database = firebase.database();
        this.uid = 'null';
        this.checkUid();

    }
    
    setUid = () =>this.uid=this.auth.currentUser.uid;
    singInWithEmail = async (email,password) =>
    {
        this.signOut();
        await this.auth.signInWithEmailAndPassword(email,password);
        this.setUid();
        this.email=email;
        return(this.uid);
    }
    getCards = () => 
    {
        this.checkUid();
        return this.database.ref(`/${this.uid}`);
    };
    signOut = () =>
    {
        this.auth.signOut();
        this.uid = 'null';
        this.email = 'null'
        localStorage.removeItem('user');
    }
    addCard = (en, ru, wordArr)=>
    {
    this.getCards().set([
            ...wordArr,
            {
            id: +new Date(),
            en,
            ru
        }], () =>{})
    }
    deleteCard = (id, wordArr)=>
    {
        const newWordArr = wordArr.filter(item => item.id !==id);
        this.getCards().set(newWordArr);
    }
    registerUser = (email, password)=>this.auth.createUserWithEmailAndPassword(email,password);
    checkUid =() =>
    {
        if (this.uid==='null'&&this.auth.currentUser)
        {
            this.setUid();
        }
        else if(localStorage.getItem('user'))
        {
            this.uid=localStorage.getItem('user');
        }
        if (localStorage.getItem('userEmail'))
        {
            this.email=localStorage.getItem('userEmail');
        }
    }

}

export default Firebase;